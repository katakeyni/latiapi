require 'test_helper'

class LigneCommandesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ligne_commande = ligne_commandes(:one)
  end

  test "should get index" do
    get ligne_commandes_url, as: :json
    assert_response :success
  end

  test "should create ligne_commande" do
    assert_difference('LigneCommande.count') do
      post ligne_commandes_url, params: { ligne_commande: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ligne_commande" do
    get ligne_commande_url(@ligne_commande), as: :json
    assert_response :success
  end

  test "should update ligne_commande" do
    patch ligne_commande_url(@ligne_commande), params: { ligne_commande: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ligne_commande" do
    assert_difference('LigneCommande.count', -1) do
      delete ligne_commande_url(@ligne_commande), as: :json
    end

    assert_response 204
  end
end
