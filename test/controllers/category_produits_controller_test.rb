require 'test_helper'

class CategoryProduitsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @category_produit = category_produits(:one)
  end

  test "should get index" do
    get category_produits_url
    assert_response :success
  end

  test "should get new" do
    get new_category_produit_url
    assert_response :success
  end

  test "should create category_produit" do
    assert_difference('CategoryProduit.count') do
      post category_produits_url, params: { category_produit: {  } }
    end

    assert_redirected_to category_produit_url(CategoryProduit.last)
  end

  test "should show category_produit" do
    get category_produit_url(@category_produit)
    assert_response :success
  end

  test "should get edit" do
    get edit_category_produit_url(@category_produit)
    assert_response :success
  end

  test "should update category_produit" do
    patch category_produit_url(@category_produit), params: { category_produit: {  } }
    assert_redirected_to category_produit_url(@category_produit)
  end

  test "should destroy category_produit" do
    assert_difference('CategoryProduit.count', -1) do
      delete category_produit_url(@category_produit)
    end

    assert_redirected_to category_produits_url
  end
end
