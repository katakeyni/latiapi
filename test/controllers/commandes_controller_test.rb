require 'test_helper'

class CommandesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @commande = commandes(:one)
  end

  test "should get index" do
    get commandes_url, as: :json
    assert_response :success
  end

  test "should create commande" do
    assert_difference('Commande.count') do
      post commandes_url, params: { commande: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show commande" do
    get commande_url(@commande), as: :json
    assert_response :success
  end

  test "should update commande" do
    patch commande_url(@commande), params: { commande: {  } }, as: :json
    assert_response 200
  end

  test "should destroy commande" do
    assert_difference('Commande.count', -1) do
      delete commande_url(@commande), as: :json
    end

    assert_response 204
  end
end
