class AddCommandeRefToLigneCommande < ActiveRecord::Migration[5.1]
  def change
    add_reference :ligne_commandes, :commande, foreign_key: true
  end
end
