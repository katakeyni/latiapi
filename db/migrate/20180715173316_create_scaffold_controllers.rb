class CreateScaffoldControllers < ActiveRecord::Migration[5.1]
  def change
    create_table :scaffold_controllers do |t|

      t.timestamps
    end
  end
end
