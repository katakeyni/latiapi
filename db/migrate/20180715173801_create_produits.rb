class CreateProduits < ActiveRecord::Migration[5.1]
  def change
    create_table :produits do |t|
      t.string :code_produit
      t.integer :prix_unitaire
      t.string :name
      t.boolean :out_stock
      t.references :category_produit, foreign_key: true

      t.timestamps
    end
  end
end
