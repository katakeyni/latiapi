class CreateCommandes < ActiveRecord::Migration[5.1]
  def change
    create_table :commandes do |t|
      t.integer :numero_commande
      t.datetime :date_commande
      t.datetime :date_paiement
      t.boolean :status
      t.integer :total

      t.timestamps
    end
  end
end
