class CreateMenus < ActiveRecord::Migration[5.1]
  def change
    create_table :menus do |t|
      t.string :code_menu
      t.integer :prix_unitaire
      t.string :name

      t.timestamps
    end
  end
end
