class CreateLigneCommandes < ActiveRecord::Migration[5.1]
  def change
    create_table :ligne_commandes do |t|
      t.integer :prix_total
      t.integer :prix_unitaire
      t.integer :quantite

      t.timestamps
    end
  end
end
