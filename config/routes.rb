Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  devise_for :users

  scope :api do
  	resources :category_produits
  	resources :commandes
  	resources :ligne_commandes
  	resources :produits
  	resources :category_produits

  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
