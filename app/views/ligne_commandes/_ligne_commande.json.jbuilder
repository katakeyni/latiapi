json.extract! ligne_commande, :id, :created_at, :updated_at
json.url ligne_commande_url(ligne_commande, format: :json)
