json.extract! category_produit, :id, :created_at, :updated_at
json.url category_produit_url(category_produit, format: :json)
