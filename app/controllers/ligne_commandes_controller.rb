class LigneCommandesController < ApplicationController
  before_action :set_ligne_commande, only: [:show, :update, :destroy]

  # GET /ligne_commandes
  # GET /ligne_commandes.json
  def index
    @ligne_commandes = LigneCommande.all
  end

  # GET /ligne_commandes/1
  # GET /ligne_commandes/1.json
  def show
  end

  # POST /ligne_commandes
  # POST /ligne_commandes.json
  def create
    @ligne_commande = LigneCommande.new(ligne_commande_params)

    if @ligne_commande.save
      render :show, status: :created, location: @ligne_commande
    else
      render json: @ligne_commande.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ligne_commandes/1
  # PATCH/PUT /ligne_commandes/1.json
  def update
    if @ligne_commande.update(ligne_commande_params)
      render :show, status: :ok, location: @ligne_commande
    else
      render json: @ligne_commande.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ligne_commandes/1
  # DELETE /ligne_commandes/1.json
  def destroy
    @ligne_commande.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ligne_commande
      @ligne_commande = LigneCommande.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ligne_commande_params
      params.fetch(:ligne_commande, {})
    end
end
