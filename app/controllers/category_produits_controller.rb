class CategoryProduitsController < ApplicationController
  before_action :set_category_produit, only: [:show, :edit, :update, :destroy]

  # GET /category_produits
  # GET /category_produits.json
  def index
    @category_produits = CategoryProduit.all
  end

  # GET /category_produits/1
  # GET /category_produits/1.json
  def show
  end

  # GET /category_produits/new
  def new
    @category_produit = CategoryProduit.new
  end

  # GET /category_produits/1/edit
  def edit
  end

  # POST /category_produits
  # POST /category_produits.json
  def create
    @category_produit = CategoryProduit.new(category_produit_params)

    respond_to do |format|
      if @category_produit.save
        format.html { redirect_to @category_produit, notice: 'Category produit was successfully created.' }
        format.json { render :show, status: :created, location: @category_produit }
      else
        format.html { render :new }
        format.json { render json: @category_produit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /category_produits/1
  # PATCH/PUT /category_produits/1.json
  def update
    respond_to do |format|
      if @category_produit.update(category_produit_params)
        format.html { redirect_to @category_produit, notice: 'Category produit was successfully updated.' }
        format.json { render :show, status: :ok, location: @category_produit }
      else
        format.html { render :edit }
        format.json { render json: @category_produit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /category_produits/1
  # DELETE /category_produits/1.json
  def destroy
    @category_produit.destroy
    respond_to do |format|
      format.html { redirect_to category_produits_url, notice: 'Category produit was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category_produit
      @category_produit = CategoryProduit.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_produit_params
      params.fetch(:category_produit, {})
    end
end
