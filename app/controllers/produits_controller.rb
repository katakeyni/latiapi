class ProduitsController < ApplicationController
  before_action :set_produit, only: [:show, :update, :destroy]

  # GET /produits
  # GET /produits.json
  def index
    @produits = Produit.all
    render json: @produits
  end

  # GET /produits/1
  # GET /produits/1.json
  def show
  end

  # POST /produits
  # POST /produits.json
  def create
    @produit = Produit.new(produit_params)

    if @produit.save
      render :show, status: :created, location: @produit
    else
      render json: @produit.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /produits/1
  # PATCH/PUT /produits/1.json
  def update
    if @produit.update(produit_params)
      render :show, status: :ok, location: @produit
    else
      render json: @produit.errors, status: :unprocessable_entity
    end
  end

  # DELETE /produits/1
  # DELETE /produits/1.json
  def destroy
    @produit.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_produit
      @produit = Produit.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def produit_params
      params.fetch(:produit, {})
    end
end
