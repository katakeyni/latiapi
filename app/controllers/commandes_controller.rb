class CommandesController < ApplicationController
  before_action :set_commande, only: [:show, :update, :destroy]

  # GET /commandes
  # GET /commandes.json
  def index
    @commandes = Commande.all
  end

  # GET /commandes/1
  # GET /commandes/1.json
  def show
  end

  # POST /commandes
  # POST /commandes.json
  def create
    @commande = Commande.new(commande_params)

    if @commande.save
      render :show, status: :created, location: @commande
    else
      render json: @commande.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /commandes/1
  # PATCH/PUT /commandes/1.json
  def update
    if @commande.update(commande_params)
      render :show, status: :ok, location: @commande
    else
      render json: @commande.errors, status: :unprocessable_entity
    end
  end

  # DELETE /commandes/1
  # DELETE /commandes/1.json
  def destroy
    @commande.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_commande
      @commande = Commande.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def commande_params
      params.fetch(:commande, {})
    end
end
